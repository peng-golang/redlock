package redlock

import (
	"errors"
)

var ErrWaitLockTimeout = errors.New("wait lock timeout")
