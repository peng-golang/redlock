package redlock

import (
	"github.com/stretchr/testify/mock"
	"time"
)

type MockRedLock struct {
	mock.Mock
}

func (m *MockRedLock) TryLock(key string, ttl time.Duration) error {
	args := m.Called(key, ttl)
	return args.Error(0)
}

func (m *MockRedLock) ReleaseLock(key string) error {
	args := m.Called(key)
	return args.Error(0)
}
