package redlock

import (
	"context"
	"errors"
	"fmt"
	"github.com/redis/go-redis/v9"
	"time"
)

type IRedLock interface {
	TryLock(key string, ttl time.Duration) error
	ReleaseLock(key string) error
	TryLockWithTimeout(key string, ttl time.Duration, timeout time.Duration) error
}

type RedLock struct {
	ctx         context.Context
	redisClient *redis.Client
	prefix      string
	WaitTime    time.Duration
}

func NewRedLock(ctx context.Context, redisClient *redis.Client, prefix string) *RedLock {
	return &RedLock{redisClient: redisClient, ctx: ctx, prefix: prefix, WaitTime: time.Millisecond * 10}
}

func (l RedLock) TryLock(key string, ttl time.Duration) error {
	ctx, cancel := context.WithTimeout(l.ctx, time.Second*3)
	rs := false
	var err error
	defer cancel()
	for !rs {
		rs, err = l.redisClient.SetNX(ctx, fmt.Sprintf("%s:rdl:%s", l.prefix, key), "locked", ttl).Result()
		if errors.Is(err, context.DeadlineExceeded) {
			return ErrWaitLockTimeout
		} else if err != nil {
			return err
		}
		if rs {
			return nil
		}
		time.Sleep(l.WaitTime)
	}
	return nil
}

func (l RedLock) TryLockWithTimeout(key string, ttl time.Duration, timeout time.Duration) error {
	ctx, cancel := context.WithTimeout(l.ctx, timeout)
	rs := false
	var err error
	defer cancel()
	for !rs {
		rs, err = l.redisClient.SetNX(ctx, fmt.Sprintf("%s:rdl:%s", l.prefix, key), "locked", ttl).Result()
		if errors.Is(err, context.DeadlineExceeded) {
			return ErrWaitLockTimeout
		} else if err != nil {
			return err
		}
		if rs {
			return nil
		}
		time.Sleep(l.WaitTime)
	}
	return nil
}

func (l RedLock) ReleaseLock(key string) error {
	_, err := l.redisClient.Del(l.ctx, fmt.Sprintf("%s:rdl:%s", l.prefix, key)).Result()
	return err
}
