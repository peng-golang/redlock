package redlock

import (
	"context"
	"github.com/redis/go-redis/v9"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

var redClient *redis.Client

func init() {
	redClient = redis.NewClient(&redis.Options{
		Addr:     "127.0.0.1:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
}

func TestRedLock_TryLock_WaitTimeout(t *testing.T) {
	rl := NewRedLock(context.Background(), redClient, "test")
	err := rl.TryLock("test", time.Second*20)
	defer rl.ReleaseLock("test")
	if err != nil {
		t.Error(err)
	}

	err = rl.TryLock("test", time.Second*2)
	assert.ErrorIs(t, err, ErrWaitLockTimeout)
}

func TestRedLock_TryLock_Success(t *testing.T) {
	rl := NewRedLock(context.Background(), redClient, "test")
	defer rl.ReleaseLock("test")
	err := rl.TryLock("test", time.Second*2)
	if err != nil {
		t.Error(err)
	}

	err = rl.TryLock("test", time.Second*2)
	if err != nil {
		t.Error(err)
	}
}

func TestRedLock_TryLock_ReleaseSuccess(t *testing.T) {
	rl := NewRedLock(context.Background(), redClient, "test")
	defer rl.ReleaseLock("test")
	err := rl.TryLock("test", time.Second*20)
	assert.NoError(t, err)

	go func() {
		time.Sleep(time.Second * 2)
		err = rl.ReleaseLock("test")
		assert.NoError(t, err)
	}()

	err = rl.TryLock("test", time.Second*2)
	assert.NoError(t, err)
}
